<%-- 
    Document   : index
    Created on : 08-05-2021, 20:36:01
    Author     : Alonso
--%>


<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.evafinal.entity.HistorialPalabras"%>
<%@page import="com.mycompany.evafinal.entity.HistorialPalabras"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
      List<HistorialPalabras> lista   = (List<HistorialPalabras>) request.getAttribute("lista");
    Iterator<HistorialPalabras> itHistorialPalabras = lista.iterator();
%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
                  
                <form  name="form" action="PalabrasController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>Palabra</th>
                    <th> Fecha</th>
             
                    <th> </th>
        
                    </thead>
                    <tbody>
                        <%while (itHistorialPalabras.hasNext()) {
                      HistorialPalabras per = itHistorialPalabras.next();%>
                        <tr>
                            <td><%= per.getPalabra()%></td>
                            <td><%= per.getFecha()%></td>
                          
                            <td> <input type="radio" name="seleccion" value="<%= per.getId()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
              <button type="submit" name="accion" value="ver" class="btn btn-success">ver solicitud</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>

        
        </form>   
                
  
    </body>
</html>
