/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.evafinal.controller;


import com.mycompany.evafinal.PalabraDTO;
import com.mycompany.evafinal.dao.HistorialPalabrasJpaController;
import com.mycompany.evafinal.entity.HistorialPalabras;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Alonso
 */
@WebServlet(name = "PalabrasController", urlPatterns = {"/PalabrasController"})
public class PalabrasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PalabrasController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PalabrasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String accion = request.getParameter("accion");
         System.out.println(accion);
                 
         if (accion.equals("Ingreso")) {
            request.getRequestDispatcher("ingreso.jsp").forward(request, response);

        }
          if (accion.equals("registrarsolicitud")) {
            HistorialPalabras palabra = new HistorialPalabras ();
            palabra.setPalabra(request.getParameter("palabra"));
            int numero = (int) Math.floor(Math.random()*(1-20000+1)+20000);
            String id= "A";
            
            palabra.setId(id+numero);
            
            Date fecha = new Date();
            palabra.setFecha(fecha);
          
            
            
            Client client = ClientBuilder.newClient();
            WebTarget myResource1 = client.target("https://evafinalciisa.herokuapp.com/api/historial");
           

            HistorialPalabras palabra1 = myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(palabra), HistorialPalabras.class);

            request.getRequestDispatcher("index.jsp").forward(request, response);

        }
               if (accion.equals("listar")) {
             Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("https://evafinalciisa.herokuapp.com/api/historial");

                List<HistorialPalabras> lista = (List<HistorialPalabras>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<HistorialPalabras>>() {
            });
               request.setAttribute("lista", lista);
               request.getRequestDispatcher("lista.jsp").forward(request, response);
            
        }
               
                if (accion.equals("eliminar")) {
          String idEliminar=request.getParameter("seleccion");
           Client client1 = ClientBuilder.newClient();
            WebTarget myResource1 = client1.target("https://evafinalciisa.herokuapp.com/api/historial/"+idEliminar);
            myResource1.request(MediaType.APPLICATION_JSON).delete();

         request.getRequestDispatcher("index.jsp").forward(request, response);
        }
                if (accion.equals("ver")) {
         System.out.print("antes de llamar a api");
             String idConsultar=request.getParameter("seleccion"); 
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("https://evafinalciisa.herokuapp.com/api/historial/"+idConsultar);

              HistorialPalabras palabra= myResource.request(MediaType.APPLICATION_JSON).get(HistorialPalabras.class);
       System.out.print("despues de llamar a api");
               request.setAttribute("palabra", palabra);
               request.getRequestDispatcher("ver.jsp").forward(request, response);
             
             
         }
          
          
       
          
         

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
