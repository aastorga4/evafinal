/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evafinal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alonso
 */
@Entity
@Table(name = "historial_palabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialPalabras.findAll", query = "SELECT h FROM HistorialPalabras h"),
    @NamedQuery(name = "HistorialPalabras.findById", query = "SELECT h FROM HistorialPalabras h WHERE h.id = :id"),
    @NamedQuery(name = "HistorialPalabras.findByPalabra", query = "SELECT h FROM HistorialPalabras h WHERE h.palabra = :palabra"),
    @NamedQuery(name = "HistorialPalabras.findByFecha", query = "SELECT h FROM HistorialPalabras h WHERE h.fecha = :fecha")})
public class HistorialPalabras implements Serializable {

    @Size(max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id")
    private String id;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public HistorialPalabras() {
    }

    public HistorialPalabras(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialPalabras)) {
            return false;
        }
        HistorialPalabras other = (HistorialPalabras) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.evafinal.entity.HistorialPalabras[ id=" + id + " ]";
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }
    
}
