/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evafinal;



/**
 *
 * @author Alonso
 */
public class Metadata {
     private String operation;
  private String provider;
  private String schema;


 // Getter Methods 

  public String getOperation() {
    return operation;
  }

  public String getProvider() {
    return provider;
  }

  public String getSchema() {
    return schema;
  }

 // Setter Methods 

  public void setOperation( String operation ) {
    this.operation = operation;
  }

  public void setProvider( String provider ) {
    this.provider = provider;
  }

  public void setSchema( String schema ) {
    this.schema = schema;
  }
}
