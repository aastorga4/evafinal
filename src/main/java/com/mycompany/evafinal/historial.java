/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evafinal;

import com.mycompany.evafinal.dao.HistorialPalabrasJpaController;
import com.mycompany.evafinal.dao.exceptions.NonexistentEntityException;
import com.mycompany.evafinal.entity.HistorialPalabras;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Alonso
 */
@Path("historial")
public class historial {
    
  
    
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes(){
  
        
       HistorialPalabrasJpaController dao= new HistorialPalabrasJpaController();
        List<HistorialPalabras> lista= dao.findHistorialPalabrasEntities();
       return Response.ok(200).entity(lista).build();
    }
       @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idbuscar") String idbusca) {

        HistorialPalabrasJpaController dao = new HistorialPalabrasJpaController();

        HistorialPalabras pal = dao.findHistorialPalabras(idbusca);
       System.out.print("rut devuelto"+pal.getId());
        return Response.ok(200).entity(pal).build();

    }
  
 
    
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON) 
    public Response add(HistorialPalabras h){
       
       try {
             HistorialPalabrasJpaController dao= new HistorialPalabrasJpaController();
            dao.create(h);
        } catch (Exception ex) {
            Logger.getLogger(HistorialPalabras.class.getName()).log(Level.SEVERE, null, ex);
        }
        
           return Response.ok(200).entity(h).build();
        
    }
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON) 
   public Response delete(@PathParam("iddelete") String iddelete){
       
        try {
             HistorialPalabrasJpaController dao= new HistorialPalabrasJpaController();
            
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(HistorialPalabras.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return Response.ok("cliente eliminado").build();
    }
    
      @PUT
    public Response update(HistorialPalabras h){
        
        try {
             HistorialPalabrasJpaController dao= new HistorialPalabrasJpaController();
            dao.edit(h);
       } catch (Exception ex) {
            Logger.getLogger(HistorialPalabras.class.getName()).log(Level.SEVERE, null, ex);
       }
        
          return Response.ok(200).entity(h).build();
    }
   
    
}
