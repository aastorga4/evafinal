/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evafinal.dao;

import com.mycompany.evafinal.dao.exceptions.NonexistentEntityException;
import com.mycompany.evafinal.dao.exceptions.PreexistingEntityException;
import com.mycompany.evafinal.entity.HistorialPalabras;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;

/**
 *
 * @author Alonso
 */

public class HistorialPalabrasJpaController implements Serializable {

    public HistorialPalabrasJpaController() {
        
    }
    private EntityManagerFactory emf =  Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(HistorialPalabras historialPalabras) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(historialPalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHistorialPalabras(historialPalabras.getId()) != null) {
                throw new PreexistingEntityException("HistorialPalabras " + historialPalabras + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(HistorialPalabras historialPalabras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            historialPalabras = em.merge(historialPalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = historialPalabras.getId();
                if (findHistorialPalabras(id) == null) {
                    throw new NonexistentEntityException("The historialPalabras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            HistorialPalabras historialPalabras;
            try {
                historialPalabras = em.getReference(HistorialPalabras.class, id);
                historialPalabras.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historialPalabras with id " + id + " no longer exists.", enfe);
            }
            em.remove(historialPalabras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<HistorialPalabras> findHistorialPalabrasEntities() {
        return findHistorialPalabrasEntities(true, -1, -1);
    }

    public List<HistorialPalabras> findHistorialPalabrasEntities(int maxResults, int firstResult) {
        return findHistorialPalabrasEntities(false, maxResults, firstResult);
    }

    private List<HistorialPalabras> findHistorialPalabrasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HistorialPalabras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public HistorialPalabras findHistorialPalabras(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HistorialPalabras.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistorialPalabrasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HistorialPalabras> rt = cq.from(HistorialPalabras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
